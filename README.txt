--- Summary ---
Only nodes allowed.
An easy way to create blocks based on a node view mode.
Allowed one block per view mode per node type.
Block must be placed on the node page for it to be rendered.
This module was created to help content editors to manage their content on a page from one location.

Scenario:
There are 3 regions that will host content on the node detail page:
1. banner
2. main content
3. sidebar

Your main content is already inserted into the system main content block.
But what about your sidebar and banner content?
If all this content can/will be managed from the node you could create a view mode for the others regions.
Assign the fields you need in each of those view modes.
Enable node view mode block for those view modes.
Place the blocks into the desired regions.

Need to create new view modes?
Try https://www.drupal.org/project/themer_view_modes

--- How to use ---

- Enable the Node View Mode Block setting on the display page (example: /admin/structure/types/manage/article/teaser)
- Place block into desired region that will be visible on the node detail page (example: /admin/structure/block)
- Visit a node detail page matching the Node View Mode Block (example: article page)
- To disable, simply go back to the display page settings and uncheck the setting (example: /admin/structure/types/manage/article/teaser)